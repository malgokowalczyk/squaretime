import com.google.common.base.Stopwatch;

public class SquareTime {

    public static void main(String[]args){

        final Stopwatch stopwatch = Stopwatch.createUnstarted();
        stopwatch.start();
        calculateSquareMath(7896.0);
        stopwatch.stop();
        System.out.println("Math square ==> " + stopwatch);
        stopwatch.reset();
        stopwatch.start();
        calculateSquareMulti(7896.0);
        stopwatch.stop();
        System.out.println("Multiplication square ==> " + stopwatch);

    }

    static double calculateSquareMath(double x){
        return Math.pow(x,2);
    }

    static double calculateSquareMulti(double x){
        return x*x;
    }


}
